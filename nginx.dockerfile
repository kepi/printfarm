FROM nginx:1.19.4

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
COPY proxy_params /etc/nginx
COPY .htpasswd /etc/nginx/conf/htpasswd

WORKDIR /newsletter
