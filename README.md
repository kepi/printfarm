# printfarm
Dieses Repo enthält alles was man braucht, um den octoprintserver für die Ultimaker aufzusetzen und zu warten.

## Verwendung
Die 3 Drucker sind an 3 Octoprint Instanzen angeschlossen. Die Instanzen sind von 1-3 durchnummeriert. Jede Instanz ist über einen eigenen Link zu erreichen:

http://[ip-des-octoprintservers]/octoprint1

http://[ip-des-octoprintservers]/octoprint2

http://[ip-des-octoprintservers]/octoprint3

Die Instanzen sind doppelt Login geschützt. Ein Login läuft über den Nginx Webserver (wird zuerst abgefragt) und der andere wird von der jeweiligen Octoprint Instanz verwaltet.

Alle Instanzen benutzen die selbe Kamera.

**Vorsicht:** Die Drucker sind beschriftet. Wenn die USB-Kabel am Server umsteckt, ändert sich eventuell welcher Drucker an welchem Link hängt.

## Setup
Auf dem Server ist Ubuntu 20 mit docker und docker-compose installiert.

Auf dem Server sind alle wichtigen Dateien im Verzeichnis `~dev/printfarm`.

Folgende Befehle sind notwendig, um das Setup so wie es jetzt ist zu erstellen:

```shell
cd ~dev/printfarm
```

Udev rules einstellen. Diese bestimmen, welcher Drucker an welchen Port angeschlossen ist.
```shell
sudo cp udev.rules /etc/udev/rules.d/10-usb-default.rules
sudo udevadm trigger
```
Hiernach sollte der Server einmal neu gestartet werden.

Login-Daten für nginx anlegen:

Install requirements
```
sudo apt-get install apache2-utils
```

Generate user - password pairs
```
sudo htpasswd -c .htpasswd new_user
```
or if the password file already exists
```
sudo htpasswd .htpasswd new_user
```

Bevor wir docker starten, müssen wir jetzt noch die Drucker anschalten.
```shell
sudo docker-compose up -d
```
Alles startet automatisch neu, wenn der Server mal heruntergefahren werden sollte. Wenn der Startet, muss nur darauf geachtet werden, dass alle Drucker an sind.

Um docker zu beenden kann folgender Befehl ausgefürt werden
```shell
sudo docker-compose down
```
